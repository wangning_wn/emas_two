//
//  AliHAACCSMessageService.h
//  AliHATBAdapter
//
//  Created by hansong.lhs on 2018/1/2.
//

#import <Foundation/Foundation.h>
#import <AliHAProtocol/AliHAProtocol.h>

@interface AliHAACCSMessageService : NSObject <AliHARemoteDebugMessageProtocol>

+ (AliHAACCSMessageService *)sharedInstance;

- (void)initWithDomain:(NSString *)domain serviceId:(NSString *)serviceId;

/**
 * bind accs service
 */
- (void)bindAccs:(void (^)(RemoteDebugRequest *))commandHandler;

@end
